function switchTab(i){
	switch(i){
		case 0:
			document.getElementById('addtab').disabled = true;
			document.getElementById('uptab').disabled = false;
			document.getElementById('edittab').disabled = false;
			document.getElementById('post').style.display = "inline";
			document.getElementById('upload').style.display = "none";
			document.getElementById('edit').style.display = "none";
			break;
		case 1:
			document.getElementById('uptab').disabled = true;
			document.getElementById('addtab').disabled = false;
			document.getElementById('edittab').disabled = false;
			document.getElementById('post').style.display = "none";
			document.getElementById('upload').style.display = "inline";
			document.getElementById('edit').style.display = "none";
			break;
		case 2:
			document.getElementById('uptab').disabled = false;
			document.getElementById('addtab').disabled = false;
			document.getElementById('edittab').disabled = true;
			document.getElementById('post').style.display = "none";
			document.getElementById('upload').style.display = "none";
			document.getElementById('edit').style.display = "inline";
	}
}