<?
$db_host = "localhost";
$db_user = "sample_user"; //same as in ../config.php
$db_password = "pass";
$db_name = "sample_db";
// search strings for str_replace()
$search_arr = array("\n", "[img]", "[/img]", "[b]", "[/b]");
// replace strings
$repl_arr = array("<br>", "<img src=\"", "\">", "<b>", "</b>");
// Style file (.css)
$style = "../css/main.css";
?>