<?php
// Database Name
$db_name = "sample_db";
// Database User
$db_user = "user";
// User password
$db_password = "pass";
// Database host (leave localhost if db server and http server are controlled by same host)
$db_host = "localhost";
// Blog name (displayed in header)
$blog_name="Sample blog";
// Style (.css) file
$style = "css/main.css";
// Used by <title>
$title = "John Johnson's blog"
?>